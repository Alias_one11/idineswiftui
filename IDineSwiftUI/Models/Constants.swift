import UIKit

struct Constants {
    // MARK: @Properties
    static let DairyKey = "D"
    static let GlutenKey = "G"
    static let NutsKey = "N"
    static let SoyKey = "S"
    static let VeganKey = "V"
}
