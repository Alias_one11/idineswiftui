import SwiftUI

struct CheckoutView: View {
    // MARK: @Properties
    @EnvironmentObject var order: Order
    
    // payment types
    static let listOfPayments = [ "Cash", "Credit Card", "iDine Points" ]
    static let listOfTipAmts = [ 10, 15, 20, 25, 0 ]
    
    // MARK: - @State Binding our data
    @State private var paymentType: Int = 0
    @State private var loyaltyDetails: Bool = false
    @State private var loyaltyNumber: String = ""
    @State private var tipAmt: Int = 1
    @State private var showingPaymentAlert: Bool = false
    
    // MARK: - Computed-property
    var totalPrice: Double {
        let total = Double(order.total)
        let tipValue = total / Double(Self.listOfTipAmts[tipAmt])
        
        return total + tipValue
    }
    
    var body: some View {
        Form {
            Section {
                Picker("How would you like to pay?", selection: $paymentType) {
                    ForEach(0 ..< Self.listOfPayments.count) {
                        Text(Self.listOfPayments[$0])
                    }
                }
                // Animations makes the toggle slide in smooth!
                Toggle(isOn: $loyaltyDetails.animation()) {
                    Text("Add iDine loyalty card")
                }
                
                // Only shows if loyaltyDetails = true
                if loyaltyDetails {
                    TextField("Enter your iDine ID", text: $loyaltyNumber)
                }
            }
            
            Section(header: Text("Add a tip?")) {
                Picker("Pct: ", selection: $tipAmt) {
                    ForEach(0 ..< Self.listOfTipAmts.count) {
                        Text("\(Self.listOfTipAmts[$0])%")
                    }
                }.pickerStyle(SegmentedPickerStyle())
            }
            
            Section(
                header:
                Text("TOTAL: $\(totalPrice, specifier: "%.2f")").font(.largeTitle)
            ) {
                /**|__________|*/
                Button("Confirm order") {
                    self.showingPaymentAlert.toggle()
                }
            }
            /// ||__________||
        }.navigationBarTitle(Text("Payment"), displayMode: .inline)
            // Attaching an alert to our form
            .alert(isPresented: $showingPaymentAlert) {
                Alert(title: Text("Order Confirmed"),
                      message: Text("Your total was $\(totalPrice, specifier: "%.2f") - Thank You!"), dismissButton: .default(Text("OK")))
        }
    }
}

struct CheckoutView_Previews: PreviewProvider {
    static let order = Order()
    
    static var previews: some View {
        CheckoutView().environmentObject(order)
            .previewDevice("iphone 11 Pro Max")
            .previewLayout(.fixed(width: 450, height: 520))
    }
}
