import SwiftUI

struct ItemRow: View {
    
    // MARK: - typealias
    typealias k = Constants
    
    // MARK: - ©Properties
    /*©-----------------------------------------©*/
    let colors: [ String : Color ] = [
        k.DairyKey : .purple,
        k.GlutenKey : .black,
        k.NutsKey : .red,
        k.SoyKey : .blue,
        k.VeganKey : .green
    ]
    
    var item: MenuItem
    /*©-----------------------------------------©*/
    
    // MARK: -#Opec --> body
    var body: some View {
        // Will display > arrows in our tableview to push to a new screen
        NavigationLink(destination: ItemDetail(item: item)) {
            /// ||__________||
            HStack {// Horizontal stack-view
                circleImg()
                /// ||__________||
                VStack(alignment: .leading) {
                    Text(item.name)
                        .font(.headline)
                    /**|__________|*/
                    Text("$\(item.price)")
                }
                
                // Will space our stack horizontally
                Spacer()
                /** id: \.self:
                 Use the string in the text itself, as the identifer */
                ForEach(item.restrictions, id: \.self) { restriction in
                    self.foodRestriction(restriction)
                }
            }
        }
    }
    
    // MARK: - Class methods
    fileprivate func circleImg() -> some View {
        return Image(item.thumbnailImage)
            .clipShape(Circle())
            .overlay(Circle().stroke(Color.gray, lineWidth: 2))
    }
    
    fileprivate func foodRestriction(_ restriction: String) -> some View {
        return Text(restriction)
            .font(.caption)// Gives it a smaller font
            .fontWeight(.black)
            .padding(5)// Pads the text all around by 5 pts
            .background(self.colors[restriction, default: .black])
            .clipShape(Circle()).foregroundColor(.white)
    }
}

struct ItemRow_Previews: PreviewProvider {
    static var previews: some View {
        ItemRow(item: MenuItem.example)
            .previewDevice("iphone 11 Pro Max")
            .previewLayout(.fixed(width: 350, height: 420))
    }
}
