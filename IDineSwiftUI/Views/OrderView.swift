import SwiftUI

struct OrderView: View {
    // MARK: - ©Properties
    /*©-----------------------------------------©*/
    @EnvironmentObject var order: Order
    /*©-----------------------------------------©*/
    
    var body: some View {
        /// ||__________||
        NavigationView {
            List {
                Section {
                    ForEach(order.items) { item in
                        HStack {
                            Text(item.name)
                            Spacer()
                            Text("$\(item.price)")
                        }
                    }.onDelete(perform: deleteItems)
                }
                /**|__________|*/
                Section {
                    NavigationLink(destination: CheckoutView()) {
                        Text("Place order")
                    // Wont segue to the next view if no order was placed
                    }.disabled(order.items.isEmpty)
                }
            }.navigationBarTitle("Order")
                .listStyle(GroupedListStyle())
                .navigationBarItems(trailing: EditButton())
        }
    }
    
    // MARK: - Helper method
    func deleteItems(at offsets: IndexSet) {
        order.items.remove(atOffsets: offsets)
    }
}// END OF STRUCT

struct OrderView_Previews: PreviewProvider {
    static let order = Order()
    
    static var previews: some View {
        OrderView().environmentObject(order)
            .previewDevice("iphone 11 Pro Max")
            .previewLayout(.fixed(width: 420, height: 520))
    }
}
