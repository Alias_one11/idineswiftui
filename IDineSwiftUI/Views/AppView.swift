import SwiftUI

struct AppView: View {
    // MARK: - ©Properties
    /*©-----------------------------------------©*/
    
    
    /*©-----------------------------------------©*/
    
    var body: some View {
        // Tabs along the bottom of the screen
        TabView {
            // Tab #1
            ContentView()
                .tabItem {
                    Image(systemName: "list.dash")
                    Text("Menu")
            }
            /// ||__________||
            // Tab #2
            OrderView()
                .tabItem {
                    Image(systemName: "square.and.pencil")
                    Text("Order")
            }
        }
    }
}

struct AppView_Previews: PreviewProvider {
    static let order = Order()
    
    static var previews: some View {
        AppView().environmentObject(order)
    }
}
