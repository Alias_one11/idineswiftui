import SwiftUI

struct ItemDetail: View {
    
    // MARK: - ©Properties
    /*©-----------------------------------------©*/
    @EnvironmentObject var order: Order
    
    var item: MenuItem
    /*©-----------------------------------------©*/
    
    var body: some View {
        VStack {
            // Lays views on top of each other
            ZStack(alignment: .bottomTrailing) {
                Image(item.mainImage)
                Text("Photo: \(item.photoCredit)").padding(4)
                    .background(Color.black).font(.caption)
                    .cornerRadius(10)
                    .foregroundColor(.white)
                // Will pull in the photo credits a it on the x & y axis
                    .offset(x: -5, y: -5)
            }
            Text(item.description)
            .padding()
            // Pushes the content in the view to the top
            
            // Button to order
            Button("Order This") {
                self.order.add(item: self.item)
            }.font(.headline)// Bold font
            
            Spacer()
            
        }.navigationBarTitle(Text(item.name), displayMode: .inline)
    }
}

// Only works when previewing
struct ItemDetail_Previews: PreviewProvider {
    //  @EnvironmentObject
    static let order = Order()
    
    static var previews: some View {
        
        // Shows a simulator preview of a navigation
        NavigationView {
            ItemDetail(item: MenuItem.example).environmentObject(order)
        }
        .previewDevice("iphone 11 Pro Max")
        .previewLayout(.fixed(width: 420, height: 520))
    }
}
