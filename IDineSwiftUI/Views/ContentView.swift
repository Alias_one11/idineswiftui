import SwiftUI

struct ContentView: View {
    // MARK: - ©Properties
    let menu = Bundle.main.decode([MenuSection].self, from: "menu.json")
    
    // body: some View Opec return type.
    // Doesnt really matter what kind
    var body: some View {
        /// ||__________||
        // You should have a large space above the text
        // It is a navigationBar from the controller added
        NavigationView {// UINavigationController
            /**|__________|*/
            List {// UITableView
                // Menu-section
                ForEach(menu) { section in
                    Section(header: Text(section.name)) {
                        // Items in menu-section
                        ForEach(section.items) { item in
                            ItemRow(item: item)
                        }
                    }
                    
                }
            }
            .navigationBarTitle("Menu")
            .listStyle(GroupedListStyle())
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .previewDevice("iphone 11 Pro Max")
            .previewLayout(.fixed(width: 345, height: 820))
    }
}
